//
//  Team.swift
//  FootballListApp
//
//  Created by Виктор Сирик on 01.11.2021.
//

import Foundation

struct TeamModel: Codable {
    let get: String
    let results: Int
    let response: [ResponseTeam]
}

struct ResponseTeam: Codable {
    let team: TeamInfo
    let venue: Venue
}

struct TeamInfo: Codable {
    let id: Int
    let name: String
    let country: String
    let founded: Int
    let national: Bool
    let logo: String
}

struct Venue: Codable {
    
}
