//
//  DetaledInfoViewController.swift
//  FootballListApp
//
//  Created by Виктор Сирик on 01.11.2021.
//

import UIKit

class DetaledInfoViewController: UIViewController {

    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    
    var player: Response?
    var teamID: String?
    var teamInfo: ResponseTeam?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fillLabels()
    }
    
    func setImage(imageURL: String) -> UIImage? {
        var image: UIImage?
        guard let url = URL(string: imageURL) else {
            return nil
        }
        do{
            let data = try Data(contentsOf: url, options: [])
            image = UIImage(data: data)
        } catch {
            print(error.localizedDescription)
        }
        return image
    }
    
    func setTeam() {
        var urlComponents = URLComponents(string: "https://api-football-beta.p.rapidapi.com/teams")!
        
        let idParametr = URLQueryItem(name: "id", value: teamID)
        urlComponents.queryItems = [idParametr]

        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": "api-football-beta.p.rapidapi.com",
                                       "x-rapidapi-key": "57fb1b76a9msh46392529f8fdcebp1b2bc0jsn05cc081835ca"]
        
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let error = error {
                print(error.localizedDescription)
            }
            
            if let data = data{
                do{
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(TeamModel.self, from: data)
                    self.teamInfo = response.response[0]
                    DispatchQueue.main.async {
                        self.photoImage.image = self.setImage(imageURL: (self.teamInfo?.team.logo) as! String)
                        self.nameLabel.text =  self.teamInfo?.team.name
                        self.countryLabel.text = self.teamInfo?.team.country
                        self.birthdayLabel.text = String(describing: self.teamInfo!.team.founded)
                    }
                } catch let error as NSError{
                    print(error.localizedDescription)
                }
            }
            
        }.resume()
    }

    
    func fillLabels() {
        
        if player != nil {
            photoImage.image = setImage(imageURL: (player?.player.photo) as! String)
            nameLabel.text =  player?.player.name
            countryLabel.text = player?.player.birth.country
            birthdayLabel.text = player?.player.birth.date
        } else if teamID != nil {
            setTeam()

        }
            
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
