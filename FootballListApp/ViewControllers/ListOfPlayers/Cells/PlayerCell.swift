//
//  PlayerCell.swift
//  FootballListApp
//
//  Created by Виктор Сирик on 31.10.2021.
//

import UIKit

protocol ShowDetaledInfoDelegate {
    func showInfoAboutPlayer(index: Int)
    func showInfoAboutTeam(index: Int)
}

class PlayerCell: UITableViewCell {

    @IBOutlet weak var namePlayerLabel: UILabel!
    @IBOutlet weak var surnamePlayerLabel: UILabel!
    @IBOutlet weak var imagePlayerImage: UIImageView!
    @IBOutlet weak var teamLogoImage: UIImageView!
    var delegate: ShowDetaledInfoDelegate?
    
    func setImage(imageURL: String) -> UIImage? {
        var image: UIImage?
        guard let url = URL(string: imageURL) else {
            return nil
        }
        do{
            let data = try Data(contentsOf: url, options: [])
            image = UIImage(data: data)
        } catch {
            print(error.localizedDescription)
        }
        return image
    }
    
    func update(playerInfo: Response) {
        namePlayerLabel.text = playerInfo.player.firstname
        surnamePlayerLabel.text = playerInfo.player.lastname
        imagePlayerImage.image = setImage(imageURL: playerInfo.player.photo)
        teamLogoImage.image = setImage(imageURL: playerInfo.statistics[0].team.logo)
    }
    
    @IBAction func showInfoAboutPlayerAction(_ sender: Any) {
        delegate?.showInfoAboutPlayer(index: tag)
    }
    
    @IBAction func showInfoAboutTeamAction(_ sender: Any) {
        delegate?.showInfoAboutTeam(index: tag)
    }
    
}
