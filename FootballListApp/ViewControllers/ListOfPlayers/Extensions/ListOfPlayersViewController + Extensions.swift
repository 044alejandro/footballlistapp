//
//  ListOfPlayersViewController + Extensions.swift
//  FootballListApp
//
//  Created by Виктор Сирик on 31.10.2021.
//

import Foundation
import UIKit

extension ListOfPlayersViewController: UITableViewDelegate, UITableViewDataSource, ShowDetaledInfoDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playersInfo.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: playerCell, for: indexPath) as! PlayerCell
        cell.tag = indexPath.row
        cell.update(playerInfo: playersInfo[indexPath.row])
        cell.delegate = self
        return cell
    }

    func showInfoAboutPlayer(index: Int) {
        let detaledInfoVC = DetaledInfoViewController(nibName: "DetaledInfoViewController", bundle: nil)
        detaledInfoVC.player = playersInfo[index]
        navigationController?.pushViewController(detaledInfoVC, animated: true)
    }

    func showInfoAboutTeam(index: Int) {
        let detaledInfoVC = DetaledInfoViewController(nibName: "DetaledInfoViewController", bundle: nil)
        detaledInfoVC.teamID = String(playersInfo[index].statistics[0].team.id)
        navigationController?.pushViewController(detaledInfoVC, animated: true)
    }
}
