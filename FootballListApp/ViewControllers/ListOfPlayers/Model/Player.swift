//
//  Player.swift
//  FootballListApp
//
//  Created by Виктор Сирик on 31.10.2021.
//

import Foundation
import UIKit

struct PlayerModel: Codable {
    let get: String
    let paging: Paging
    let parameters: Parameters
    let response: [Response]
    let results: Int
}

struct Paging: Codable {
    let current: Int
    let total: Int
}

struct Parameters: Codable {
    let league: String
    let season: String
}

struct Response: Codable {
    let player: Player
    let statistics: [Statistics]
}

struct Player: Codable {
    let age: Int
    let birth: Birthday
    let firstname: String
    let height: String
    let id: Int
    let injured: Bool
    let lastname: String
    let name: String
    let nationality: String
    let photo: String
    let weight: String
}
struct Statistics: Codable {
    let team: Team
}

struct Birthday: Codable {
    let country: String
    let date: String
    let place: String
}

struct Team: Codable {
    let id: Int
    let logo: String
    let name: String
}
