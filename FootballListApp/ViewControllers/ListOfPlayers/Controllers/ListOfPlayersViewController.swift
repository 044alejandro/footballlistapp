//
//  ListOfPlayersViewController.swift
//  FootballListApp
//
//  Created by Виктор Сирик on 31.10.2021.
//

import UIKit

class ListOfPlayersViewController: UIViewController {

    @IBOutlet weak var listOfPlayersTableView: UITableView!
    
    let playerCell = "PlayerCell"
    var playersInfo: [Response] = []
    var league: String = ""
    var season: String = ""


    override func viewDidLoad() {
        super.viewDidLoad()

        listOfPlayersTableView.register(UINib(nibName: playerCell, bundle: nil), forCellReuseIdentifier: playerCell)
        listOfPlayersTableView.delegate = self
        listOfPlayersTableView.dataSource = self
        findPlayers(league: league, season: season)
    }

    func findPlayers(league: String, season: String) {
        var urlComponentsPlayers = URLComponents(string: "https://api-football-beta.p.rapidapi.com/players/topscorers")!
        
        let leagueParametr = URLQueryItem(name: "league", value: league)
        let seasonParametr = URLQueryItem(name: "season", value: season)

        urlComponentsPlayers.queryItems = [leagueParametr, seasonParametr]
        
        var requestPlayers = URLRequest(url: urlComponentsPlayers.url!)
        requestPlayers.httpMethod = "GET"
        requestPlayers.allHTTPHeaderFields = ["x-rapidapi-host": "api-football-beta.p.rapidapi.com",
                                       "x-rapidapi-key": "57fb1b76a9msh46392529f8fdcebp1b2bc0jsn05cc081835ca"]
        
        URLSession.shared.dataTask(with: requestPlayers) { data, response, error in
            
            if let error = error {
                print(error.localizedDescription)

            }
            
            if let data = data{
                do{
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(PlayerModel.self, from: data)
                    response.response.forEach{ player in
                        self.playersInfo.append(player)
                    }
                    DispatchQueue.main.async {
                        self.listOfPlayersTableView.reloadData()
                    }
                } catch let error as NSError{
                    print(error.localizedDescription)
                }
                
            }
            
        }.resume()
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
