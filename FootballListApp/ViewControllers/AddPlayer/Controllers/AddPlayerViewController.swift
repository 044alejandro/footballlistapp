//
//  AddPlayerViewController.swift
//  FootballListApp
//
//  Created by Виктор Сирик on 31.10.2021.
//

import UIKit

class AddPlayerViewController: UIViewController {

    @IBOutlet weak var leagueTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    var league = ""
    var season = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func checkData() -> Bool{
        guard let leagueData = leagueTextField.text, let seasonData = yearTextField.text, !leagueData.isEmpty, !seasonData.isEmpty else {
            return false
        }
        league = leagueData
        season = seasonData
        return true
    }
    


    @IBAction func showPlayersAction(_ sender: Any) {
        if !checkData() {
            let alertVC = UIAlertController(title: "Error", message: "Error data", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alertVC.addAction(cancelAction)
            present(alertVC, animated: true, completion: nil)
        }
        let playersListVC = ListOfPlayersViewController(nibName: "ListOfPlayersViewController", bundle: nil)
        playersListVC.league = league
        playersListVC.season = season
            self.navigationController?.pushViewController(playersListVC, animated: true)
        
    }
}
